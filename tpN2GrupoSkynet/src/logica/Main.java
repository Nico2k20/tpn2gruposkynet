package logica;

import java.util.ArrayList;

public class Main {

	protected static boolean sonIguales(int[] esperado, ArrayList<Integer> resultado) {
		if (esperado.length != resultado.size())
			return false;

		for (Integer res : resultado) {
			if (!resultado.contains(res))
				return false;
		}

		return true;
	}

	public static void main(String[] args) {
		Grafo g = new Grafo(9);
		// A=0,B=1,C=2,D=3,E=4,F=5,G=6,H=7,I=8
		g.agregarArista(0, 1, 4);
		g.agregarArista(0, 8, 8);

		g.agregarArista(1, 2, 8);
		g.agregarArista(1, 7, 12);

		g.agregarArista(2, 3, 6);
		g.agregarArista(2, 5, 4);
		g.agregarArista(2, 8, 3);

		g.agregarArista(3, 4, 9);
		g.agregarArista(3, 5, 13);

		g.agregarArista(4, 5, 10);

		g.agregarArista(5, 6, 3);

		g.agregarArista(6, 7, 1);
		g.agregarArista(6, 8, 5);

		g.agregarArista(7, 8, 6);

		System.out.println("ORIGINAL:" + g);

		Grafo agm = AGM.generarAGM(g);
		System.out.println("ARBOL:" + agm);

		Grafo regiones = agm.generarRegiones(2);
		System.out.println("Grafo Resultante: \n" + regiones);

		System.out.println(Estadisticas.mostrarEstadisticas(regiones));

	}

}
