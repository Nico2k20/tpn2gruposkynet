package visual;

import mediador.MediadorLogica_visual;

public class Main {

	public static void main(String[] args) {

		Mapa mapa = new Mapa();
		MediadorLogica_visual mediador = new MediadorLogica_visual();

		Interfaz_Grafica gui = new Interfaz_Grafica(mapa, mediador);
		manejoDeEventosMouse eventosMouse = new manejoDeEventosMouse(gui, mapa);
		mapa.addMouseListener(eventosMouse);
	}
}