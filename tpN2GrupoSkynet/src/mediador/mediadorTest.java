package mediador;

import static org.junit.Assert.*;

import org.junit.Test;

import logica.Grafo;

public class mediadorTest {

	@Test
	public void test() {
		
		assertTrue(true);
	}

	@Test  (expected = IllegalArgumentException.class)
	public void testGrafoValido() {
		MediadorLogica_visual med = new MediadorLogica_visual(); 
		
		med.agregarNVertices(-1);
	}
	
	@Test  
	public void testGrafoAgregarVertices() {
		MediadorLogica_visual med = new MediadorLogica_visual(); 
		
		med.agregarNVertices(10);
		
		
		assertTrue(med.getCantidadVertices()==10);
		
		
	}
	
	
	@Test  
	public void testGrafoAgregarArista() {
		MediadorLogica_visual med = new MediadorLogica_visual(); 
		
		med.agregarNVertices(10);
		
		med.agregarArista(0, 1, 20);
		
		
		
		assertTrue(med.existeArista(0, 1));
		
		
	}
	
	@Test
	public void testgenerarAGM() {
		MediadorLogica_visual med = new MediadorLogica_visual();
		
		grafoCompletoConexo3V(med);
		
		med.crearAGM();
		
		
		assertTrue(med.getCantAristasAGM()==2);
		
		
	}
	@Test 
	public void testgenerarAGMVerticesAislados() {
		MediadorLogica_visual med = new MediadorLogica_visual();
		
		grafoVerticesAislados(med);
		
		assertFalse(med.crearAGM());
		}

	@Test 
	public void testgenerarAGM1Arista() {
		MediadorLogica_visual med = new MediadorLogica_visual();
		
		grafo1arista(med);
		
		assertFalse(med.crearAGM());
		}
	
	@Test 
	public void testgenerarKMenosUno() {
		MediadorLogica_visual med = new MediadorLogica_visual();
		
		this.grafoCompletoConexo3V(med);
		med.crearAGM();
		
	
		
		assertTrue(med.restarKmenos1Aristas(2));
		}
	
	@Test 
	public void testNoGenerarKMenos1() {
		MediadorLogica_visual med = new MediadorLogica_visual();
		
		this.grafo1arista(med);
	
		
		
		
		assertFalse(med.restarKmenos1Aristas(2));
		}
	
	
	@Test 
	public void testEnteroKMayorACantVertices() {
		MediadorLogica_visual med = new MediadorLogica_visual();
		
		this.grafoCompletoConexo3V(med);
	
		this.testgenerarAGM();
		
		
		assertFalse(med.restarKmenos1Aristas(5));
		}
	
	@Test 
	public void testExisteArista() {
		MediadorLogica_visual med = new MediadorLogica_visual();
		
		this.grafoCompletoConexo3V(med);
	
		
		
		
		assertTrue(med.existeArista(0, 1));
		}
	
	@Test 
	public void testNoExisteArista() {
		MediadorLogica_visual med = new MediadorLogica_visual();
		
		med.agregarNVertices(3);
	
		med.agregarArista(0, 2, 10);
		med.agregarArista(1, 2, 10);
		
		
		
		assertFalse(med.existeArista(0, 1));
		}
	
	
	

	private void grafo1arista(MediadorLogica_visual med) {
		med.agregarNVertices(3);
		med.agregarArista(0, 2, 12);
	}
	
	
	
	
	private void grafoVerticesAislados(MediadorLogica_visual med) {
		med.agregarNVertices(4);
	}
	
	
	

	private void grafoCompletoConexo3V(MediadorLogica_visual med) {
		med.agregarNVertices(3);
		
		med.agregarArista(0, 1, 1);
		med.agregarArista(0, 2, 10);
		med.agregarArista(1, 2, 4);
	}
	

	
	
	
	


}
