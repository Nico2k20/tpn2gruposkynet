package logica;

public class Estadisticas {

	public static String mostrarEstadisticas(Grafo g) {
		if (g == null) {
			throw new NullPointerException("El grafo ingresado no existe.");
		}
		if (g.getCantidadDeVertices() == 0) {
			return "Grafo vacio.";
		}
		if (g.getAristas().size() == 0) {
			if (g.getCantidadDeVertices() == 1)
				return "El grafo se conforma de " + g.getCantidadDeVertices() + " vertice aislado, no posee aristas.";
			else
				return "El grafo se conforma de " + g.getCantidadDeVertices() + " vertices aislados, no posee aristas.";
		}
		StringBuilder sb = new StringBuilder();
		int contador = 1;
		Arista mayorPeso = aristaDeMayorPeso(g);
		Arista menorPeso = aristaDeMenorPeso(g);
		
		sb.append("La arista de mayor peso del grafo esta entre los vertices " + mayorPeso.getExtremo1().getId() + " y "
				+ mayorPeso.getExtremo2().getId() + ", con peso " + mayorPeso.getPeso() + "\n");
		

		sb.append("La arista de menor peso del grafo esta entre los vertices " + menorPeso.getExtremo1().getId() + " y "
				+ menorPeso.getExtremo2().getId() + ", con peso " + menorPeso.getPeso() + "\n\n");

		sb.append("El promedio de vertices por region es: "+ promedioVerticesRegionesDelGrafo(g)+ "\n\n");

		for (int raiz : g.getRaices()) {
			sb.append("REGION " + contador + ":\n");
			sb.append("Vertices de la region: " + mostrarVerticesDeRegion(g, raiz));
			sb.append("Cantidad de vertices de la region " + ": " + cantidadDeVerticesDeRegion(g, raiz) + "\n");
			sb.append("Cantidad de aristas de la region " + ": " + cantidadDeAristasDeRegion(g, raiz) + "\n");
			sb.append("Peso total de la region: " + pesoRegion(g, raiz) + "\n");
			sb.append("Peso promedio de la region: " + pesoPromedioDeRegion(g, raiz) + "\n\n");

			contador++;
		}
		return sb.toString();
	}

	private static Arista aristaDeMayorPeso(Grafo g) {
		return g.aristaDeMayorPeso();

	}

	private static Arista aristaDeMenorPeso(Grafo g) {
		return g.aristaDeMenorPeso();
	}

	protected static int cantidadDeVerticesDeRegion(Grafo g, int raiz) {
		int cantidadDeVerticesDeRegion = 0;
		for (Vertice v : g.getVertices()) {
			if (v.getPadre() == raiz) {
				cantidadDeVerticesDeRegion++;
			}
		}
		return cantidadDeVerticesDeRegion;
	}

	protected static int cantidadDeAristasDeRegion(Grafo g, int raiz) {
		int sumaGrado = 0;
		for (Vertice v : g.getVertices()) {
			if (v.getPadre() == raiz) {
				sumaGrado += v.getGrado();
			}
		}
		return sumaGrado / 2;
	}

	protected static int pesoRegion(Grafo g, int raiz) {
		if (g.getCantidadDeVertices() == 0) {
			return 0;
		}
		int pesoDeRegion = 0;

		for (Arista a : g.getAristas()) {
			if (a.getExtremo1().getPadre() == raiz && a.getExtremo2().getPadre() == raiz) {
				pesoDeRegion += a.getPeso();
			}
		}

		return pesoDeRegion;
	}

	protected static StringBuilder pesoPromedioDeRegion(Grafo g, int raiz) {
		StringBuilder sb = new StringBuilder();

		if (pesoRegion(g, raiz) == 0) {
			sb.append("0");
			return sb;
		}
		double pesoPromedio = pesoRegion(g, raiz) * 1.0 / cantidadDeAristasDeRegion(g, raiz);
		String pesoPromedioCast = String.format("%.02f", pesoPromedio);
		sb.append(pesoPromedioCast);
		return sb;

	}

	protected static StringBuilder promedioVerticesRegionesDelGrafo(Grafo g) {
		StringBuilder sb = new StringBuilder();
		int sumatoriaCantVerticesRegion=0;
		int i=0;
		while(g.getRaices().size()>i) {
			sumatoriaCantVerticesRegion+=cantidadDeVerticesDeRegion(g,g.getRaices().get(i));
			i++;
			}
		double CantVerPromedio = sumatoriaCantVerticesRegion*1.0/g.getRaices().size();
		String cantVerPromedioCast = String.format("%.02f", CantVerPromedio);
		sb.append(cantVerPromedioCast);
		return sb;
	}
		
		
		
	
	
	protected static int cantidadDeVerticesDeRegion(Grafo g) {
		return g.getRaices().size();
	}
	

	protected static String mostrarVerticesDeRegion(Grafo g, int raiz) {
		StringBuilder sb = new StringBuilder("(");
		for (Vertice v : g.getVertices()) {
			if (v.getPadre() == raiz) {
				sb.append(v.getId() + ",");
			}
		}
		sb.append(")\n");
		return sb.toString();
	}

	protected static int pesoTotalGrafo(Grafo g) {
		int peso = 0;
		for (Arista a : g.getAristas())
			peso += a.getPeso();
		return peso;
	}

}
