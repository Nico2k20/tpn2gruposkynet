package visual;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class manejoDeEventosMouse implements MouseListener {
	private Interfaz_Grafica gui;
	private Mapa mapa;

	manejoDeEventosMouse(Interfaz_Grafica gui, Mapa mapa) {
		this.gui = gui;
		this.mapa = mapa;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		if (gui.estaApretadoMarcador())
			this.mapa.agregarMarcador(this.mapa.getCoordinates(e.getPoint()));

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}